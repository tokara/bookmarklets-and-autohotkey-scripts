# bookmarklets and autohotkey scripts

a collection of custom hotkeys some of which work only in conjunction with keyworded bookmarklets for gecko-based (firefox-based) web browsers, licensed under GPL v2 for libraries (https://www.gnu.org/licenses/old-licenses/lgpl-2.0.html)  
  
known issues:
- "joy to move mouse.ahk" script (included in "input mode for reading.ahk")
on some PCs moves mouse cursor to top left when no joysticks are connected
- sometimes some keys get "stuck" in pressed state - can be worked around by
closing the script from right-click menu of its tray icon and running it again
("Reload This Script" option usually works too but is less robust)
also may be fixed by switching to different input mode by pressing Ctrl+Capslock
or Ctrl+LWin+Capslock or Ctrl+` (this workaround may be less robust)
- sometimes hotkeys with Win key perform their function only after they are pressed twice or held for a few hundred milliseconds
(on older PCs or when PC is under major computational load)
- many bookmarklets require popup windows blocking to be disabled in web browser
- all system hotkeys involving "windows" key are remapped to key combinations
like ctrl+alt+windows+letter (same letter key as in system hotkey) to free
respective key combinations for custom hotkeys (mostly hotkeys for web browsers)

note that bookmarklets are not supported on webkit/blink/chromium-based web browsers (chrome/chromium, internet explorer, opera, safari), however some gotkeys that duplicate their behavior (ones invoked with CapsLock and letter key combinations) are supported on both gecko/firefox-based and webkit/blink/chromium web browsers
